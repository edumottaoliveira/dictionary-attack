EXEMPLO DE EXECUÇÃO

Em src/

Terminal0: 
rmiregistry -J-Djava.rmi.server.hostname=192.168.2.115

Terminal1:
java -Djava.rmi.server.hostname=192.168.2.115 br/inf/ufes/ppd/MasterImpl

Terminal2:
java -Djava.rmi.server.hostname=192.168.2.115 br/inf/ufes/ppd/SlaveImpl "sl01"

Terminal3:
java br/inf/ufes/ppd/Client 192.168.2.115 original.msg.cipher house
java br/inf/ufes/ppd/ClientTester out_file 192.168.2.115

# Executar Testes:
> java br/inf/ufes/ppd/ClientTester 192.168.2.115 out_file
 ou com argumentos forcando opcoes de usar mensagens demo e processar localmente:
> java br/inf/ufes/ppd/ClientTester 192.168.2.115 out_file demo local


