JFLAGS = -g -d
JAVAC = javac
JVM = java 
GLASSFISH_CLI = glassfish5/glassfish/lib/gf-client.jar

.SUFFIXES: .java .class
.java.class: $(JC) $(JFLAGS) $*.java

BIN = ./bin
SRC = ./src/br/inf/ufes/ppd/

COMPILE = $(JAVAC) $(JAVAFLAGS)


#JAVA_FILES = $(subst $(SRC), $(EMPTY), $(wildcard $(SRC)*.java))
JAVA_FILES = \
		Guess.java \
        Attacker.java \
		SlaveManager.java \
		Master.java \
		Slave.java \
        Decrypt.java \
        Encrypt.java \
		AttackData.java \
		SubAttackData.java \
		SlaveData.java \
		Client.java \
 		SequentialAttack.java \
		MasterImpl.java \
		SlaveImpl.java \
		ClientTester.java 

CLASS_FILES = $(JAVA_FILES:.java=.class)

all: #$(addprefix $(BIN), $(CLASS_FILES))
	javac -cp src:java-json.jar:$(GLASSFISH_CLI) $(JFLAGS) $(BIN) $(addprefix $(SRC), $(JAVA_FILES))
	#javac $(addprefix $(BIN), $(JAVA_FILES))

#$(BIN)%.class: $(SRC)%.java
#	$(COMPILE) $<

clean: rm -rf $(BIN)/br
