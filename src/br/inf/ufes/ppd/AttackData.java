package br.inf.ufes.ppd;

import java.util.List;
import java.util.ArrayList;
import br.inf.ufes.ppd.Guess;

/**
 * AttackData.java
 */ 

class AttackData {

    private byte[] cipherText;
	private byte[] knownText;
	private int activeSubAttacks;
	private List<Guess> guesses;

	public AttackData(byte[] cipherText, byte[] knownText) {
		this.cipherText = cipherText;
		this.knownText = knownText;
		activeSubAttacks = 0;
		guesses = new ArrayList<Guess>();
	}

	public byte[] getCipherText() {
		return cipherText;
	}

	public byte[] getKnownText() {
		return knownText;
	}

	public void incrementActiveSubAttacks() {
		activeSubAttacks += 1;
	}

	public void decrementActiveSubAttacks() {
		activeSubAttacks -= 1;
	}

	public void setNoActiveSubAttacks() {
		activeSubAttacks = 0;
	}

	public boolean isAttackFinished() {
		return activeSubAttacks == 0;
	}

	public int getActiveSubAttacks() {
		return activeSubAttacks;
	}

	public synchronized void addGuess(Guess guess) {
		for (Guess g: guesses) {
			if (g.getKey().equals(guess.getKey())){
				System.out.println("Guess com chave \"" + guess.getKey() + "\" ja adicionado" );
				return;
			}
		}
		guesses.add(guess);
	}

	public synchronized List<Guess> getGuesses() {
		return guesses;
	}

	public synchronized Guess[] getGuessesArray() {
		return guesses.toArray(new Guess[guesses.size()]);
	}

}
