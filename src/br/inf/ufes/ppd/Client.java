package br.inf.ufes.ppd;

/**
 * Client.java
 */

//Rmi
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
//Structures and data types
import java.util.Arrays;
import java.util.List;
//IO
import java.io.*;
import java.util.stream.*;
import java.nio.file.*;
//Cyptography
import javax.crypto.Cipher;
import javax.crypto.spec.*;
//Local
import br.inf.ufes.ppd.Guess;
//Others
import java.util.Random;


public class Client {

    // Tag de configuracao para ativar o ataque distribuido ou sequencial
    private static final boolean distributedAttack = true;

    public byte[] readFile(String filename) throws IOException {
		File file = new File(filename);
	    InputStream is = new FileInputStream(file);
        long length = file.length();
        // creates array (assumes file length<Integer.MAX_VALUE)
        byte[] data = new byte[(int)length];
        int offset = 0; int count = 0;
        while ((offset < data.length) &&
        		(count=is.read(data, offset, data.length-offset)) >= 0) {
            offset += count;
        }
        is.close();
        return data;
    }

    public void saveFile(String filename, byte[] data) throws IOException {
		FileOutputStream out = new FileOutputStream(filename);
	    out.write(data);
	    out.close();
	}
    
    public byte[] createRandomByteArray(int size) {
        int arraySize;
        Random rand = new Random();
        arraySize = (size > 0) ? size : rand.nextInt(99001) + 1000;
        byte[] array = new byte[arraySize];
        rand.nextBytes(array);

        System.out.println("Criando mensagem aleatoria de tamanho " + arraySize);
        return array;
    }

    public List<String> loadDictionary(String dictionaryFileName) {
        String workingDir = System.getProperty("user.dir");
        Path path = Paths.get(workingDir + "/" + dictionaryFileName);
        try (Stream<String> lines = Files.lines(path)) {
            return lines.collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println("Failed to load dictionary.");
            System.exit(-1);
            return null;
        }
    }

    public byte[] getRandomKeyFromDictionary(String dictionaryFileName) {
        List<String> dictionary = loadDictionary(dictionaryFileName);
        Random rand = new Random();
        int randomIndex = rand.nextInt(dictionary.size());
        byte[] key = dictionary.get(randomIndex).getBytes();
        System.out.println("Encriptando com a chave \""+ dictionary.get(randomIndex) + "\" (indice "+ randomIndex +" do dicionario)");
        return key;
    }

    public byte[] writeKnownWordInMessage(byte[] knownWord, byte[] msg) { 
        Random rand = new Random(); 
        int randomIndex = rand.nextInt(msg.length - knownWord.length); 
        for (int i = 0; i < knownWord.length; i++) { 
            msg[i + randomIndex] = knownWord[i]; 
        } 
        return msg; 
    }

    public byte[] encryptMessage(byte[] msg, byte[] key) {

        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        
            return cipher.doFinal(msg);
        }
        catch (Exception e ) {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    public void printAndSaveResults(Guess[] guesses) {
        int numGuesses = guesses.length;
        if (numGuesses == 0) {
            System.out.println("\nNao foram encontradas chaves candidatas");
            return;
        }

        String printedHeader = (numGuesses) > 1? 
            " chaves candidatas encontradas: ":
            " chave candidata encontrada: ";
        System.out.println("\n" + numGuesses + printedHeader);
        for (int i = 0; i < guesses.length; i++) {
            Guess guess = guesses[i];
            System.out.println(" - " + guess.getKey());
            
            try {
                saveFile("./test_outputs/" + guess.getKey() + ".msg", guess.getMessage());
            } 
            catch (Exception e) {
                System.err.println("Client exception: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    public double executeDistributedAttack(byte[] encryptedMessage, byte[] knownWord, String host) {

        System.out.println("Buscando mestre");
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            Master master = (Master) registry.lookup("mestre");

            // Solicita o ataque ao mestre
            System.out.println("Solicitando ataque");

            double initialTime = System.nanoTime();
            Guess[] guesses = master.attack(encryptedMessage, knownWord);
            double totalSpentTime = (System.nanoTime() - initialTime)/1000000000.0;

            printAndSaveResults(guesses);
            return totalSpentTime;
            
        } 
        catch (NullPointerException e) {
            System.err.println("\nNao foi possivel realizar o ataque (nao ha escravos)");
            System.exit(-1);
            return -1;
        }
        catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
            System.exit(-1);
            return -1;
        }
    }

    public double executeSequentialAttack(byte[] encryptedMessage, byte[] knownWord, String host) {
        
        System.out.println("Realiza o ataque localmente");

        double initialTime = System.nanoTime();
        Guess[] guesses = SequentialAttack.attack(encryptedMessage, knownWord);
        double totalSpentTime = (System.nanoTime() - initialTime)/1000000000.0;

        printAndSaveResults(guesses);
        return totalSpentTime;
    }


    public static void main(String[] args) {
        // args[0] Eh o endereco do registry
        // args[1] Eh o nome do arquivo da mensagem criptografada;
        // args[2] Eh a palavra conhecida presente no arquivo criptografado;
        // args[3] (Opcional) Eh o tamanho da mensagem aleatoria a ser criada
        // se o nome do arquivo passado em args[0] nao existir.

        if (args.length < 3) {
            System.err.println("Use: java Client 192.168.1.123 crypted.cipher house ");
            return;
        }

        String registryHost = args[0];
        String encryptedFileName = args[1];
        byte[] knownWord = args[2].getBytes();
        int fileSize = (args.length > 3) ? Integer.parseInt(args[3]) : 0;
        byte[] encryptedMessage;

        Client client = new Client();

        try {
            encryptedMessage = client.readFile(encryptedFileName);
            //System.out.println("message size (bytes) = "+ encryptedMessage.length);
        }
        catch (FileNotFoundException e) {
            byte[] randomMessage = client.createRandomByteArray(fileSize);
            randomMessage = client.writeKnownWordInMessage(knownWord, randomMessage);
            byte[] randomKey = client.getRandomKeyFromDictionary("dictionary.txt");
            encryptedMessage = client.encryptMessage(randomMessage, randomKey);
            try {
                client.saveFile(encryptedFileName, encryptedMessage);
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }

        double totalTime;

        if (Client.distributedAttack) { 
            // Realiza o ataque distribuido
            totalTime = client.executeDistributedAttack(encryptedMessage, knownWord, registryHost);            
        }
        else {
            // Realiza o ataque sequencialmente (local)
            totalTime = client.executeSequentialAttack(encryptedMessage, knownWord, registryHost);
        }

        System.out.println("\nTempo Gasto = " + totalTime + " segundos" );
    }
} 