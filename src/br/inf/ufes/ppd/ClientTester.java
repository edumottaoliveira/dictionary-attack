package br.inf.ufes.ppd;

/**
 * ClientTester.java
 */

 //Rmi
import java.rmi.Remote;
//IO
import java.io.*;
//Local
import br.inf.ufes.ppd.Guess;
import br.inf.ufes.ppd.Client;

public class ClientTester {

    // Tag de configuracao para ativar o ataque distribuido ou sequencial
    private boolean distributedAttack = true;

    // Tag para definir se mensagens pre-definidas serao usadas para testes
    private boolean useDemoMsgs = true;

    // Mensagens pre-definidas para testes
    private static final String[] testFileNames = {
        "cinco_minutos.cipher",
        "culto_arte.cipher",
        "historias_sem_data.cipher",
        "descobrimento_australia.cipher",
        "dom_casmurro.cipher",
        "enchanted_india.cipher"
    };

    // Palavras conhecidas nas mensagens pre-definidas
    private static final String[] testKnownWords = {
        "tocal-o",
        "cavalheiresca",
        "Magnanimo",
        "archipelago",
        "Soltámos",
        "clutched"
    };

    // Tamanhos definidos mensagens geradas aleatoriamente
    private static final Integer[] testMsgSizes = {
        10000, 25000, 40000, 55000, 70000, 85000, 100000
    };

    public ClientTester (boolean distributedAttack, boolean useDemoMsgs) {
        this.distributedAttack = distributedAttack;
        this.useDemoMsgs = useDemoMsgs;
    }

    public void runTests(BufferedWriter out, String registryHost) {
        String msgType = (useDemoMsgs)? "mensagens demo" : "mensagens aleatórias";
        String location = (distributedAttack)? "distribuídos" : "locais";
        System.out.println("Executando testes "+location+" com "+msgType);

        if (useDemoMsgs) {
            runTestsWithDemoFiles(out, registryHost);
        }
        else {
            runTestsWithRandomFiles(out, registryHost);
        }
    }

    public void runTestsWithDemoFiles(BufferedWriter out, String registryHost) {
        Client client = new Client();
        double totalTime;

        for (int i = 0; i < testFileNames.length; i++) {

            String fileName = testFileNames[i];
            String encryptedFilePath = "./test_msgs/" + fileName;
            System.out.println("\nTESTE " + i + ": " + fileName);
            
            byte[] knownWord = testKnownWords[i].getBytes();
            byte[] encryptedMessage;
            try {
                encryptedMessage = client.readFile(encryptedFilePath);
                out.write(fileName + ", ");
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }
           
            totalTime = (distributedAttack)?
                client.executeDistributedAttack(encryptedMessage, knownWord, registryHost) :
                client.executeSequentialAttack(encryptedMessage, knownWord, registryHost);

            System.out.println("\nTempo Gasto = " + totalTime + " segundos" );
            
            try {
                out.write(totalTime +"\n");
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }
            
        }
    } 

    public void runTestsWithRandomFiles(BufferedWriter out, String registryHost) {
        Client client = new Client();
        double totalTime;

        for (int i = 0; i < testMsgSizes.length; i++) {

            int fileSize = testMsgSizes[i];
            String fileName = "random_" + fileSize/1000 + "k.cipher";
            String encryptedFilePath = "./test_msgs/" + fileName;
            System.out.println("\nTESTE " + i + ": " + fileName);
            
            byte[] randomMessage = client.createRandomByteArray(fileSize);
            byte[] knownWord = "testknownWord".getBytes();
            randomMessage = client.writeKnownWordInMessage(knownWord, randomMessage);
            byte[] randomKey = client.getRandomKeyFromDictionary("dictionary.txt");
            byte[] encryptedMessage = client.encryptMessage(randomMessage, randomKey);
            try {
                client.saveFile(encryptedFilePath, encryptedMessage);
                out.write(fileName + ", ");
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
           
            totalTime = (distributedAttack)?
                client.executeDistributedAttack(encryptedMessage, knownWord, registryHost) :
                client.executeSequentialAttack(encryptedMessage, knownWord, registryHost);
            
            System.out.println("\nTempo Gasto = " + totalTime + " segundos" );

            try {
                out.write(totalTime +"\n");
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }
            
        }
    }

    public static void main(String[] args) {

        String registryHost = (args.length < 1)? "localhost": args[0];

        String outputFileName = (args.length < 2)? "out" : args[1];

        boolean demoMsg = false;
        boolean distributedAttack = true;
        int repeat = 1;

        for (int i = 2; i < args.length; i++) {
            if ("demo".equals(args[i])) {
                demoMsg = true;
            }
            else if ("local".equals(args[i])) {
                distributedAttack = false;
            }
            else if ("repeat".equals(args[i])) {
                if (args.length > i+1) {
                    try {
                        repeat = Integer.valueOf(args[i+1]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        ClientTester tester = new ClientTester(distributedAttack, demoMsg);

        // Repete os testes um numero especificado de vezes para que seja possivel
        // normalizar variacoes em analize posterior.
        for (int i = 0; i < repeat; i++) {

            String outputFilePath;
            BufferedWriter out;

            if (repeat > 1) {
                System.out.println("\n# Execucao "+i+":");
                outputFilePath =  "./test_logs/"+outputFileName+"_execucao_"+i+".csv";
            }
            else {
                outputFilePath = "./test_logs/"+outputFileName+".csv";
            }

            try {
                out = new BufferedWriter(new FileWriter(outputFilePath));
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }

            tester.runTests(out, registryHost);

            try {
                out.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

    }


}