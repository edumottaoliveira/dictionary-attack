package br.inf.ufes.ppd;

// Rmi
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
//Structures and data types
import java.util.HashSet;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.UUID;
//Concurrency
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.lang.Runnable;
//IO
import java.util.stream.*;
import java.nio.file.*;
import java.text.DecimalFormat;
//Local
import br.inf.ufes.ppd.AttackData;
import br.inf.ufes.ppd.SubAttackData;
import br.inf.ufes.ppd.SlaveData;

/**
 * MASTER IMPLEMENTATION
 * Implementa as funcoes da interface Master e prove metodos para gerenciar
 * os ataques sobre os escravos.
 */
public class MasterImpl implements Master {

    private long dictionarySize;
    private int lastAttackNumber = 0;

    // Mapeia a id de cada escravo a sua estrutura de dados
    private Map<UUID, SlaveData> slavesData = new HashMap<UUID, SlaveData>();

    // Mapeia a id de cada ataque a sua estrutura de dados
    private Map<Integer, AttackData> attacksData = new HashMap<Integer, AttackData>();
    
    public MasterImpl() {
        dictionarySize = getDictionarySize("dictionary.txt");
        System.out.println("Tamanho do dicionário: " + dictionarySize);
        if (dictionarySize == 0) {
            System.out.println("Erro: dicionário não encontrado.");
            System.exit(-1);
        }
    }

    @Override
    public void addSlave(Slave s, String slaveName, UUID slavekey) throws java.rmi.RemoteException {
        
        if (!slavesData.containsKey(slavekey)) {
            SlaveData slaveData = new SlaveData(s, slaveName);
            synchronized(slavesData) {
                slavesData.put(slavekey, slaveData);
            }
        }
        else {
            SlaveData slaveData = slavesData.get(slavekey);
            slaveData.setLastTimeChecked(System.currentTimeMillis());
        }
        System.out.println("Registrado \"" + slaveName + "\"");
    }   

    // Repassa sub ataques cancelados a outros escravos, se possivel
    public void redistributeSubAttacks(List<SubAttackData> subAttacks) {

        if (slavesData.size() == 0) {
            System.err.println("Trabalhos cancelados pois não há escravos registrados");

            for (SubAttackData entry : subAttacks) {
                Integer attackNumber = entry.getAttackId();
                synchronized(attacksData) {
                    attacksData.get(attackNumber).decrementActiveSubAttacks();
                }
            }
            return;
        }

        int i = 0;
        int size = subAttacks.size();
        while (true) {
            
            Map<UUID, SlaveData> slavesDataCopy;
            synchronized(slavesData) {
                slavesDataCopy = new HashMap<UUID, SlaveData>(slavesData);
            }

            // Distribui trabalhos entre os escravos registrados
            for (SlaveData slaveData : slavesDataCopy.values()) {
                if (i >= size) {
                    return;
                }
                SubAttackData subAttack = subAttacks.get(i);
                Integer attackId = subAttack.getAttackId();
                long initialWordIndex = subAttack.getCurrentWordIndex();
                long finalWordIndex = subAttack.getFinalWordIndex();
                System.out.println("Attack " + attackId + ": repassado para escravo \""+ slaveData.getName()+"\"");
                System.out.println("Attack " + attackId + ": criado sub ataque de "+ initialWordIndex +
                    " a "+ finalWordIndex);
                SubAttackData newSubAttack = new SubAttackData(initialWordIndex, finalWordIndex, attackId);

                synchronized(slaveData) {
                    slaveData.addSubAttackData(newSubAttack);
                }
                slaveData.setLastTimeChecked(System.currentTimeMillis());
                Slave slave = slaveData.getSlave();

                AttackData attackData;
                synchronized(attacksData) {
                    attackData = attacksData.get(attackId);
                }

                byte[] ciphertext = attackData.getCipherText();
                byte[] knowntext = attackData.getKnownText();
            
                try {
                    slave.startSubAttack(ciphertext, knowntext, initialWordIndex, 
                        finalWordIndex, attackId, this);
                }
                catch (RemoteException e ) {
                    System.err.println("Ataque " + attackId +": Escravo inacessível para executar slave.startSubAttack");
                    //System.err.println(e.toString());
                    System.err.println("Cancelando sub ataque...");
                    synchronized(attackData) {
                        attackData.decrementActiveSubAttacks();
                        // attackData.notify();
                    }
                }
                i++;
            }
            
        }
    }

    @Override
    public void removeSlave(UUID slaveKey) throws java.rmi.RemoteException {

        synchronized(slavesData) {
            if (!slavesData.containsKey(slaveKey)) {
                System.out.println("Impossível remover escravo. Escravo já removido!");
                return;
            }
    
            SlaveData slaveData = slavesData.get(slaveKey);
            List<SubAttackData> subAttacks = new LinkedList<SubAttackData>(slaveData.getSubAttacksData());
    
            System.out.println("Removendo escravo \"" + slaveData.getName() + "\"");
            try {
                slavesData.remove(slaveKey);
            } catch (Exception e) {
                e.printStackTrace();
            }
    
            if (subAttacks.size() > 0) {
                System.out.println("Redistribuindo trabalhos do escravo removido");
                redistributeSubAttacks(subAttacks); // send copy
            }
        }
    }

    @Override
    public void foundGuess(UUID slaveKey, int attackNumber, long currentindex,Guess currentguess)
        throws java.rmi.RemoteException {

        System.out.println("Ataque " + attackNumber + ": guess encontrado com chave \"" 
            + currentguess.getKey() + "\" ");
        //System.out.println(currentguess.getMessage());

        synchronized (attacksData) {
            AttackData attackData = attacksData.get(attackNumber);
            attackData.addGuess(currentguess);
        }
    }

    @Override
    public void checkpoint(UUID slaveKey, int attackNumber, long currentindex)
        throws java.rmi.RemoteException {

        synchronized(slavesData) {
            SlaveData slaveData = slavesData.get(slaveKey);

            SubAttackData subAttackData = slaveData.getSubAttackData(attackNumber, currentindex);
            if (subAttackData == null) {
                System.err.println("!!!!!Checkpoint falhou! Dados do sub ataque não encontrados");
                return;
            }
            
            subAttackData.setCurrentWordIndex(currentindex);

            long initialTime = subAttackData.getInitialTime();
            long currentTime = System.currentTimeMillis();
            double timeDiffInSeconds = (currentTime - initialTime)/1000.0;

            long indicesDiff = currentindex - subAttackData.getInitialWordIndex();
            double indicesPerSecond = indicesDiff/timeDiffInSeconds;
            DecimalFormat df = new DecimalFormat("#.##");

            boolean subAttackFinished = subAttackData.getFinalWordIndex() == currentindex;
            System.out.println("Checkpoint" +
                (subAttackFinished? " Final": "") +
                " de \"" + slaveData.getName() + 
                "\" para o ataque " + attackNumber +": " + currentindex + " em " +
                df.format(timeDiffInSeconds) + "s (" +
                df.format(indicesPerSecond) + " índices/s)");

            slaveData.setLastTimeChecked(currentTime);

            if (subAttackFinished) {
                // Subataque finalizado
                slaveData.removeSubAttackData(subAttackData);
                AttackData attackData;
                synchronized(attacksData) {
                    attackData = attacksData.get(attackNumber);
                }
                synchronized(attackData) {
                    attackData.decrementActiveSubAttacks();
                    if (attackData.isAttackFinished()) {
                        // TODO: Exception in thread "Thread-2" java.lang.IllegalMonitorStateException
                        // Sinaliza que o ataque acabou e destrava o metodo attack em espera.
                        attackData.notify();
                    }
                }
            }
            
        }
    }

    // Cria e distribui sub ataques aos escravos devidamentes registrados
    private void buildSubAttacks(Map<UUID, SlaveData> slavesDataCopy, byte[] ciphertext,
            byte[] knowntext, int attackNumber, long dictionarySlice) {

        long finalWordIndex, initialWordIndex = 0;

        // Inicia sub ataque em cada escravo
        for (Map.Entry<UUID, SlaveData> entry : slavesDataCopy.entrySet()) {
            
            UUID slaveId = entry.getKey();
            SlaveData slaveData = entry.getValue();

            finalWordIndex = initialWordIndex + dictionarySlice - 1;
            if (dictionarySize - finalWordIndex < dictionarySlice) {
                finalWordIndex = dictionarySize - 1;
            }

            SubAttackData subAttackData = new SubAttackData(initialWordIndex, finalWordIndex, attackNumber);
            Slave slave;
            synchronized(slaveData) {  
                System.out.println("Ataque "+ attackNumber + ": escravo \"" + slaveData.getName() +
                "\" recebe sub ataque de "+ initialWordIndex + " a "+ finalWordIndex);
            
                slaveData.addSubAttackData(subAttackData);
                slaveData.setLastTimeChecked(System.currentTimeMillis());
                slave = slaveData.getSlave();
            }

            synchronized(attacksData) {
                attacksData.get(attackNumber).incrementActiveSubAttacks();
            }
        
            synchronized(slave) {
                //slave.startSubAttack(ciphertext, knowntext, initialWordIndex, 
                //finalWordIndex, attackNumber, this);
                
                // Inicia uma thread separada para chamar o sub ataque
                Runnable subAttack = new SubAttackManager(slave, ciphertext, knowntext, 
                        subAttackData, this);
                Thread subAttackThread = new Thread(subAttack); 
                subAttackThread.start(); 
            }             

            initialWordIndex = finalWordIndex + 1;
        }
    }

    @Override
    public Guess[] attack(byte[] ciphertext, byte[] knowntext) throws RemoteException {
        
        int attackNumber = lastAttackNumber++;
        System.out.println("Ataque "+ attackNumber + ": iniciado");

        AttackData newAttackData = new AttackData(ciphertext, knowntext);
        synchronized(attacksData) {
            attacksData.put(attackNumber, newAttackData);
        }

        int numSlaves;
        Map<UUID, SlaveData> slavesDataCopy;
        synchronized (slavesData) {
            slavesDataCopy = new HashMap<UUID, SlaveData>(slavesData);            
        }
        numSlaves = slavesData.size();
        if (numSlaves == 0) {
            System.out.println("Ataque "+ attackNumber + " cancelado: Nao há escravos registrados!");
            return null;
        }
        long dictionarySlice = dictionarySize / numSlaves;
        buildSubAttacks(slavesDataCopy, ciphertext, knowntext, attackNumber, dictionarySlice);
 
        Guess[] guessesArray = null;
        try {
            System.out.println("Ataque "+ attackNumber + ": aguardando completar...");
            synchronized (newAttackData) {
                // Bloqueia ate ser notificado que os sub ataques foram finalizados
                newAttackData.wait();
                guessesArray = newAttackData.getGuessesArray();
            }
        } catch (Exception e ) {
            e.printStackTrace();
        }

        synchronized(slavesData) {
            attacksData.remove(attackNumber);
        }

        System.out.println("Ataque "+ attackNumber + ": finalizado!");
        return guessesArray;
    }

    private long getDictionarySize(String dictionaryFileName) {
        String workingDir = System.getProperty("user.dir");
        Path path = Paths.get(workingDir + "/" + dictionaryFileName);
        try (Stream<String> lines = Files.lines(path)) {
            long numOfLines = lines.count();
            return numOfLines;
        } catch (Exception e) {
            System.err.println("Erro ao obter o tamanho do dicionário");
            System.err.println(" Detalhes: "+ e.toString());
            return 0;
        }
    }

    public Map<UUID, SlaveData> getSlavesData () {
        return slavesData;
    }

    public static void main(String[] args) {

        String host = (args.length < 2) ? "localhost" : args[0];

        try {
        	MasterImpl master = new MasterImpl();
            Master masterRef = (Master) UnicastRemoteObject.exportObject(master, 0);
            // Bind the remote object in the registry
            System.err.println("Buscando registry");
            Registry registry = LocateRegistry.getRegistry(host); // opcional: host
			registry.rebind("mestre", masterRef);
            System.err.println("Mestre pronto!");

            final Runnable slaveMonitor = new SlaveMonitor(master);
            final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(slaveMonitor, 0, 10, TimeUnit.SECONDS);

        } catch (Exception e) {
            System.err.println("Exceção inicializando mestre: " + e.toString());
            e.printStackTrace();
        }
    }
}




/**
 * SLAVE MONITOR
 * Monitora os escravos ativos e repassa sub ataques dos que se tornaram inativos
 */
class SlaveMonitor implements Runnable {

    MasterImpl master;
    Map<UUID, SlaveData> slavesData;

    public SlaveMonitor(MasterImpl master) {
        this.master = master;
        this.slavesData = master.getSlavesData();
    }

    private void removeSlave(Master master, UUID slaveId) {
        try {
            master.removeSlave(slaveId);
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void run()  {
        //System.err.println("Slaves monitor");
        
        try {

            Map<UUID, SlaveData> slavesDataCopy;
            synchronized(slavesData) {
                slavesDataCopy = new HashMap<UUID, SlaveData>(slavesData);
            }

            for (Map.Entry<UUID, SlaveData> entry : slavesDataCopy.entrySet()) {
                UUID slaveId = entry.getKey();
                SlaveData slaveData = entry.getValue();

                long timeDiff = System.currentTimeMillis() - slaveData.getLastTimeChecked(); 
                double timeDiffInSeconds = timeDiff/1000.0;
                DecimalFormat df = new DecimalFormat("#.##");

                System.out.print("Monitor \"" + slaveData.getName() +
                        "\": " + df.format(timeDiffInSeconds) + "s");

                if (slaveData.countActiveSubAttacks() > 0) {
                    System.out.print("\n");
                    
                    if (timeDiffInSeconds > 20) {
                    removeSlave(master, slaveId);
                    }
                }
                else {
                    System.out.print(" (ocioso)\n");

                    if (timeDiffInSeconds > 40) {
                    removeSlave(master, slaveId);
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}




/**
 * SUB ATTACK MANAGER
 * Executa a chamada startSubAttack nos escravos
 */
class SubAttackManager implements Runnable { 
 
    Slave slave; 
    byte[] cipherText, knownText; 
    SubAttackData subAttackData;
    MasterImpl master;

    public SubAttackManager(Slave slave, byte[] cipherText, byte[] knownText, 
        SubAttackData subAttackData, MasterImpl master) { 
         
        this.slave = slave; 
        this.cipherText = cipherText; 
        this.knownText = knownText; 
        this.subAttackData = subAttackData;
        this.master = master;
    }
    
    public void run() { 

        long initialWordIndex = subAttackData.getInitialWordIndex(); 
        long finalWordIndex = subAttackData.getFinalWordIndex(); 
        int attackNumber = subAttackData.getAttackId();
        SlaveManager callbackInterface = master;  
        try { 
            slave.startSubAttack(cipherText, knownText, initialWordIndex,  
                finalWordIndex, attackNumber, callbackInterface); 
        } 
        catch (RemoteException e) {
            System.err.println("Ataque "+ attackNumber +": Escravo inacessível para executar slave.startSubAttack");
            //System.err.println(" Exceção: "+ e.toString());

            List<SubAttackData> subAttackList = new ArrayList<SubAttackData>();
            subAttackList.add(subAttackData);
            System.out.println("Redistribuindo sub ataque do escravo inacessível");
            master.redistributeSubAttacks(subAttackList);
            //e.printStackTrace();
        }
    } 
}