package br.inf.ufes.ppd;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.*;
import java.nio.file.*;
import javax.crypto.Cipher;
import javax.crypto.spec.*;
import br.inf.ufes.ppd.Guess;

public class SequentialAttack {

    public static List<String> loadDictionary(String dictionaryFileName) {
        String workingDir = System.getProperty("user.dir");
        Path path = Paths.get(workingDir + "/" + dictionaryFileName);
        try (Stream<String> lines = Files.lines(path)) {
            return lines.collect(Collectors.toList());
        } catch (Exception e) {
            System.out.println("Failed to load dictionary.");
            System.exit(-1);
            return null;
        }
    }

    private static boolean isPatternPresent(byte[] pattern, byte[] array) {
        int arrayLen = array.length;
        int patternLen = pattern.length;
        if (arrayLen < patternLen) {
            return false;
        }

        int i, j, k;
        for (i = 0; i <= arrayLen - patternLen; i++) {
            k = 0;
            j = i;
            while (k < patternLen && pattern[k] == array[j]) {
                j++;
                k++;
            }           
            if (k == patternLen) {
                 return true;
            }
        }
        return false;
    }

    private static byte[] decryptMessage(byte[] key, byte[] ciphertext) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            byte[] decrypted = cipher.doFinal(ciphertext);
            return decrypted;

        } catch (javax.crypto.BadPaddingException e) {
			// essa excecao e jogada quando a senha esta incorreta
			// porem nao quer dizer que a senha esta correta se nao jogar essa excecao
            return null;

		} catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static Guess testWordAsKey(String word, byte[] ciphertext, byte[] knowntext) {

        byte[] decrypted = decryptMessage(word.getBytes(), ciphertext);        

        if (decrypted != null && isPatternPresent(knowntext, decrypted)) {
            Guess guess = new Guess();
            guess.setKey(word);
            guess.setMessage(decrypted);
            System.out.println("Found guess! key = " + word);
            return guess;
        }
        return null;
    }

    public static Guess[] attack(byte[] ciphertext, byte[] knowntext) {

        List<Guess> guesses = new ArrayList<Guess>();

        List<String> dictionary = loadDictionary("dictionary.txt");
        long initialWordIndex = 0;
        long finalWordIndex = dictionary.size() - 1;

        for (long i = initialWordIndex; i <= finalWordIndex; i++ ) {
            int index = (int) i;
            String word = dictionary.get(index);
            Guess guess = testWordAsKey(word, ciphertext, knowntext);
            if (guess != null) {
                guesses.add(guess);
            }
        }

        Guess[] guessesArray = guesses.toArray(new Guess[guesses.size()]);
        return guessesArray;
    }


}