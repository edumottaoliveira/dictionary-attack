package br.inf.ufes.ppd;

import br.inf.ufes.ppd.SubAttackData;
import java.util.List;
import java.util.LinkedList;

/**
 * SlaveData.java
 */ 

class SlaveData {
	
    private Slave slave;
    private String name;
    private long lastTimeChecked; // Em milisegundos
    private List<SubAttackData> subAttacks;

	public SlaveData(Slave slave, String name) {
		this.slave = slave;
        this.name = name;
        lastTimeChecked = System.currentTimeMillis();
        subAttacks = new LinkedList<SubAttackData>();
	}

	public Slave getSlave() {
		return slave;
    }
    
    public String getName() {
        return name;
    }

	public long getLastTimeChecked() {
		return lastTimeChecked;
	}

	public void setLastTimeChecked(long t) {
		lastTimeChecked = t;
	}
    
    public synchronized void addSubAttackData(SubAttackData subAttackData) {
        subAttacks.add(subAttackData);
    }

    public synchronized SubAttackData getSubAttackData(Integer attackId, long currentIndex) {
        for (SubAttackData s: subAttacks) {
            if (s.getAttackId() == attackId &&
                currentIndex >= s.getInitialWordIndex() && 
                currentIndex <= s.getFinalWordIndex()) {
                    return s;
            }
        }
        return null;
    }

    public synchronized void removeSubAttackData(SubAttackData subAttack) {
        subAttacks.remove(subAttack);
    }

    public synchronized List<SubAttackData> getSubAttacksData() {
        return subAttacks;
    }

    public synchronized int countActiveSubAttacks() {
        return subAttacks.size();
    }

}
