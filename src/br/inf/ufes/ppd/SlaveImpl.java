package br.inf.ufes.ppd;

// Rmi
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
// Structures and data types
import java.util.List;
import java.util.UUID;
// Concurrency
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit.*;
import java.lang.Runnable;
// IO
import java.util.stream.*;
import java.nio.file.*;
// Cryptography
import javax.crypto.Cipher;
import javax.crypto.spec.*;
// Local
import br.inf.ufes.ppd.Guess;

/**
* SLAVE IMPLEMENTATION
* Instancia um escravo, conecta-o com um mestre e processa sub ataques
* recebidos paralelamente, enviando ao mestre checkpoints periodicos e 
* guesses encontrados.
*/
public class SlaveImpl implements Slave {
  
    private UUID id;
    private String name, host;
    private Master master;
    private Slave slaveRef;
    private List<String> dictionary;

    public SlaveImpl(String name, Master master, String host) {
        this.name = name;
        this.id = java.util.UUID.randomUUID();
        this.master = master;
        this.host = host;
        loadDictionary("dictionary.txt");
    }

    public String getName() {
        return name;
    }

    public UUID getId() {
        return id;
    }

    public Master getMaster() {
        return master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    public String getHost() {
        return host;
    }

    public Slave getSlaveRef() {
        return slaveRef;
    }

    public void setSlaveRef(Slave slaveRef) {
        this.slaveRef = slaveRef;
    }

    public void loadDictionary(String fileName) {
        String workingDir = System.getProperty("user.dir");
        Path path = Paths.get(workingDir + "/" + fileName);
        try (Stream<String> lines = Files.lines(path)) {
            dictionary = lines.collect(Collectors.toList());
        } catch (Exception e) {
            System.err.println("Erro ao carregar dicionário!");
            System.err.println(" Exceção: " + e.toString());
        }
    }
    
    public List<String> getDictionary() {
        return dictionary;
    }
    
    @Override
    public void startSubAttack(byte[] ciphertext, byte[] knowntext,
        long initialwordindex, long finalwordindex, int attackNumber, 
        SlaveManager callbackinterface) throws java.rmi.RemoteException {

        System.out.println("Ataque " + attackNumber + ": iniciando sub ataque de "+ initialwordindex + " a "+ finalwordindex);

        final Runnable slaveAttack = new SlaveAttack(this, ciphertext, knowntext,
            initialwordindex, finalwordindex, attackNumber, callbackinterface);
        Thread thread = new Thread(slaveAttack);
        thread.start();
    }

    // Inicia a execucao periodica do registro no mestre
    static private void scheduleRegistration(SlaveImpl slave) {

        final Runnable slaveRegistration = new SlaveRegistration(slave);
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        // Executa o registro do escravo a cada 30s
        scheduler.scheduleAtFixedRate(slaveRegistration, 0, 30, TimeUnit.SECONDS);
    }

    public static void main(String[] args) {

        String name = (args.length < 1) ? "Escravo_teste" : args[0];
        String host = (args.length < 2) ? "localhost" : args[1];
        System.out.println("Escravo "+ name + " iniciado");

        System.out.println("Buscando mestre");
		try {
		    Registry registry = LocateRegistry.getRegistry(host);
            Master master = (Master) registry.lookup("mestre");
            System.out.println("Mestre encontrado");

            SlaveImpl slave = new SlaveImpl(name, master, host);
            Slave slaveRef = (Slave) UnicastRemoteObject.exportObject(slave, 0);
            slave.setSlaveRef(slaveRef);
            
            System.out.println("Iniciando registro");
            scheduleRegistration(slave);

            // Captura Ctrl+c e desregistra o escravo
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    System.out.println(" Desregistrando escravo \"" + name + "\"");
                    try {
                        master.removeSlave(slave.getId());
                    } catch (RemoteException e) {
                        System.err.println("Exceção ao desregistrar escravo: " + e.toString());
                    }
                }
             });
        } 
        catch (Exception e) {
		    System.err.println("Exceção ao iniciar escravo: " + e.toString());
		    e.printStackTrace();
		}
    }    
}




/**
 * SLAVE ATTACK
 * Processa um sub ataque, enviando checkpoints e guesses encontrados a um mestre
 */
class SlaveAttack implements Runnable {

    SlaveImpl slave;
    private byte[] cipherText;
    private byte[] knownText;
    private long initialWordIndex;
    private long finalWordIndex;
    private int attackNumber;
    private SlaveManager callbackInterface;
    private long currentWordIndex;

    public SlaveAttack(SlaveImpl slave, byte[] ciphertext, byte[] knowntext, long initialwordindex, 
        long finalwordindex, int attackNumber, SlaveManager callbackinterface) {
        this.slave = slave;
        this.cipherText = ciphertext;
        this.knownText = knowntext;
        this.initialWordIndex = initialwordindex;
        this.finalWordIndex = finalwordindex;
        this.attackNumber = attackNumber;
        this.callbackInterface = callbackinterface;
        currentWordIndex = initialwordindex;
    }

    public long getCurrentWordIndex() {
        return currentWordIndex;
    }

    public SlaveImpl getSlave() {
        return slave;
    }

    // Inicia a execucao de checkpoint no mestre a cada 10 segundos, 
    // durande a execucao do sub ataque.
    private ScheduledExecutorService scheduleCheckpoint(int attackNumber, long currentWordIndex) {

        final Runnable checkpointSender = new SlaveCheckpoint(this, attackNumber);
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        // Checkpoint deve ser executado a cada 10s  
        scheduler.scheduleAtFixedRate(checkpointSender, 0, 10, TimeUnit.SECONDS);
        return scheduler;
    }

    private void sendFoundGuess(Guess guess) {
        Master master = slave.getMaster();
        try {
            master.foundGuess(slave.getId(), attackNumber, currentWordIndex, guess);
        }
        catch (RemoteException e) {
            System.out.println("Mestre inacessível para executar master.foundGuess");
            System.err.println(" Exceção: " + e.toString());
            // TODO: Tentar enviar guess novamente depois de um intervalo
        }
    }

    // TODO: Boyer–Moore string search algorithm pode ser uma solucao melhor
    // Verifica se uma array de bytes esta presente dentro de outro array de bytes 
    private boolean isPatternPresent(byte[] pattern, byte[] array) {
        int arrayLen = array.length;
        int patternLen = pattern.length;
        if (arrayLen < patternLen) {
            return false;
        }

        int i, j, k;
        for (i = 0; i <= arrayLen - patternLen; i++) {
            k = 0;
            j = i;
            while (k < patternLen && pattern[k] == array[j]) {
                j++;
                k++;
            }           
            if (k == patternLen) {
                 return true;
            }
        }
        return false;
    }

    private byte[] decryptMessage(byte[] key) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            byte[] decrypted = cipher.doFinal(this.cipherText);
            return decrypted;

        } catch (javax.crypto.BadPaddingException e) {
			// essa excecao e jogada quando a senha esta incorreta
			// porem nao quer dizer que a senha esta correta se nao jogar essa excecao
            return null;

		} catch (Exception e) {
            System.err.println("Erro ao descriptografar mensagem!");
            System.err.println(" Exceção: " + e.toString());
            //e.printStackTrace();
            return null;
        }
    }

    // Verifica se string pode ser chave de criptografia
    private void testWordAsKey(String word) {

        byte[] decrypted = decryptMessage(word.getBytes());        

        if (decrypted != null && isPatternPresent(knownText, decrypted)) {
            Guess guess = new Guess();
            guess.setKey(word);
            guess.setMessage(decrypted);
            System.out.println("Encontrado guess! key = " + word);
            sendFoundGuess(guess);
        }
    }

    public void run() {

        ScheduledExecutorService scheduler;
        scheduler = scheduleCheckpoint(attackNumber, currentWordIndex);
        
        List<String> dictionary = slave.getDictionary();

        for (long i = initialWordIndex; i <= finalWordIndex; i++ ) {
            int index = (int) i;
            String word = dictionary.get(index);
            testWordAsKey(word);
            currentWordIndex += 1;
        }

        // Desativa envio de checkpoints para esse sub ataque
        scheduler.shutdown();

        // Envia ultimo checkpoint
        try {
            callbackInterface.checkpoint(slave.getId(), attackNumber, finalWordIndex);
            System.out.println("Checkpoint final do ataque " + attackNumber +": índice final = " + finalWordIndex);
        }
        catch (RemoteException e) {
            System.err.println("Checkpoint final do ataque " + attackNumber +" falhou!");
            System.err.println(" Exceção: " + e.toString());
            //e.printStackTrace();
        }
    }
}



/*
* SLAVE CHECKPOINT
* Envia checkpoint a um mestre
*/
class SlaveCheckpoint implements Runnable {

    String host;
    SlaveImpl slave;
    UUID slaveId;
    int attackNumber;
    SlaveAttack slaveAttack;
    
    public SlaveCheckpoint(SlaveAttack slaveAttack, int attackNumber) {
        SlaveImpl slave = slaveAttack.getSlave();
        this.host = slave.getHost();
        this.slave = slave;
        this.slaveId = slave.getId();
        this.attackNumber = attackNumber;
        this.slaveAttack = slaveAttack;
    }

    public void run() {
        try {
            long currentIndex = slaveAttack.getCurrentWordIndex();
            Master master = slave.getMaster();
            master.checkpoint(slaveId, attackNumber, currentIndex);
            System.out.println("Checkpoint ataque " + attackNumber +": índice = " + currentIndex);
        }
        catch (java.rmi.RemoteException e) {
            System.out.println("Checkpoint ataque " + attackNumber + 
                ": Falhou! Mestre não encontrado para checkpoint");
        }
    }
}




/*
* SLAVE REGISTRATION
* Registra escravo em um mestre. Se o mestre nao for encontrado
* busca uma nova referencia no servidor de registro e faz uma nova
* tenttiva. 
*/
class SlaveRegistration implements Runnable {

    String host, slaveName;
    Master master;
    SlaveImpl slave;
    Slave slaveRef;
    UUID slaveId;

    public SlaveRegistration(SlaveImpl slave) {
        this.slave = slave;
        this.host = slave.getHost();
        this.master = slave.getMaster();
        this.slaveRef = slave.getSlaveRef();
        this.slaveName = slave.getName();
        this.slaveId = slave.getId();
    }

    private void reconect() {
        System.out.println("Buscando o mestre novamente");
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            master = (Master) registry.lookup("mestre");
            slave.setMaster(master);

            master.addSlave(slaveRef, slaveName, slaveId);
            System.out.println("Registrado no mestre");
        } 
        catch (java.rmi.RemoteException r) {
            System.out.println("Registro falhou: Mestre não encontrado");
        } 
        catch (Exception r) {
        }
    }

    public void run() {
        try {
            master.addSlave(slaveRef, slaveName, slaveId);
            System.out.println("Registrado no mestre");
        } 
        catch (java.rmi.RemoteException e) {

            System.out.println("Registro falhou: Mestre não encontrado");
            reconect();
            //e.printStackTrace();
        }
	}
}