package br.inf.ufes.ppd;

/**
 * SubAttackData.java
 */ 

class SubAttackData {
	
    private long initialWordIndex;
	private long finalWordIndex;
    private long currentWordIndex;
    private int attackId;
    private long initialTime; // Em milisegundos

	public SubAttackData(long initialWordIndex, long finalWordIndex, int attackId) {
		this.initialWordIndex = initialWordIndex;
        this.finalWordIndex = finalWordIndex;
        this.attackId = attackId;
        currentWordIndex = initialWordIndex;
		initialTime = System.currentTimeMillis();
	}

	public long getInitialWordIndex() {
		return initialWordIndex;
	}

	public long getFinalWordIndex() {
		return finalWordIndex;
	}

	public long getCurrentWordIndex() {
		return currentWordIndex;
	}

	public void setCurrentWordIndex(long i) {
		currentWordIndex = i;
	}

	public long getInitialTime() {
		return initialTime;
	}

	public int getAttackId() {
		return attackId;
	}

}
